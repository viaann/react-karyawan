import React from "react";
import { Router, Switch, Route } from "react-router-dom";
import Home from "./components/Home";
import Register from "./components/Register";
import Nav from "./components/Nav";

function App() {
    return (
        <>
          <Nav/>
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/register" exact component={Register} />
            </Switch>
        </>
    );
}

export default App;