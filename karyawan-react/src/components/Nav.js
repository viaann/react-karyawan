import React, { Component } from "react";
import "../style/Nav.css";

class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <>
                <nav className="navbar navbar-expand-lg navbar-light">
                  <a className="navbar-brand nav m-3" href="/">Employee</a>
                  <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                  <div className="navbar-nav">
                    <a className="nav-item nav-link navItem" href="/">Home</a>
                    <a className="nav-item nav-link navItem" href="/register">Register</a>
                  </div>
                </div>
                </nav>                 
            </>
        );
    }
}

export default Nav;
