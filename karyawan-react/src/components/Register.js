import React, { Component } from 'react'
const axios = require('axios');

class Employee extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            nik: "",
            nama: "",
            jabatan: "",
            ttl: "",
            jenisKelamin: "",
            alamat: "",
            rtRw: "",
            kelDesa: "",
            kecamatan: "",
            agama: "",
            status: "",
            pekerjaan: "",
            kewarganegaraan: "",
            position: ""
        }
    }

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = () => {
        const salary = this.state.position === "Karyawan" ? "Rp 3000000" : "Rp 10000000"
        const reqDetail = {
            nik: parseInt(this.state.nik),
            nama: this.state.nama,
            tempat_tgl: this.state.ttl,
            jenis_kelamin: this.state.jenisKelamin,
            alamat: this.state.alamat,
            rt_rw: this.state.rtRw,
            kel_desa: this.state.kelDesa,
            kecamatan: this.state.kecamatan,
            agama: this.state.agama,
            status: this.state.status,
            pekerjaan: this.state.pekerjaan,
            kewarganegaraan: this.state.kewarganegaraan,
        }
        
        console.log(reqDetail)

        const reqPosition = {
            employee: {
                nik: parseInt(this.state.nik)
            },
            jabatan: this.state.position
        }

        const reqSalary = {
            employee: {
                nik: parseInt(this.state.nik)
            },
            salary: salary
        }

        this.employeePost(reqDetail)
        
        setTimeout(() => {
            this.positionPost(reqPosition)
            this.salaryPost(reqSalary)
        }, 2000);
    }


    employeePost = req => {
        fetch(`http://localhost:8080/addEmployee`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(req)
        })
        .then(res => console.log(res))
        .catch(console.error);
    }

    positionPost = req => {
        fetch(`http://localhost:8080/addPosition`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(req)
        })    
    }

    salaryPost = req => {
        fetch(`http://localhost:8080/addSalary`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(req)
        })    
        .then(data => window.location.reload())   
    }


    render() { 
        return ( 
            <>
                <div className="container-fluid container-dashboard">
                    <p className="ml-3 dashboard-title">Add new Employee</p>
                    <div className="ml-3" style={{width: "70%"}}>
                        <div class="form-group">
                            <label className="labelInput">NIK</label>
                            <input type="number" name="nik" className="form-control" id="nikInput" onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label className="labelInput">Nama</label>
                            <input type="text" className="form-control" id="namaInput" name="nama" onChange={this.handleChange}/>
                        </div>
                        <div class="form-group">
                            <label className="labelInput">Jabatan</label>
                            <select className="form-control" id="jabatanSelect" name="jabatan" onChange={this.handleChange}>
                                <option>Pilih..</option>
                                <option>Karyawan</option>
                                <option>Manager</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <label className="labelInput">Tempat/Tgl Lahir</label>
                            <input type="text" className="form-control" id="tempatInput" name="ttl" onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label className="labelInput">Jenis Kelamin</label>
                            <select className="form-control" id="jenisKelaminSelect" name="jenisKelamin" onChange={this.handleChange}>
                                <option selected>Pilih..</option>
                                <option>Laki Laki</option>
                                <option>Perempuan</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <label className="labelInput">Alamat</label>
                            <input type="text" class="form-control" id="alamatInput" name="alamat" onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label className="labelInput">RT/RW</label>
                            <input type="text" className="form-control" id="rtRwInput" name="rtRw" onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label className="labelInput">Kel/Desa</label>
                            <input type="text" className="form-control" id="kelDesaInput" name="kelDesa" onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label className="labelInput">Kecamatan</label>
                            <input type="text" className="form-control" id="kecInput" name="kecamatan" onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label className="labelInput">Agama</label>
                            <select className="form-control" id="agamaSelect" name="agama" onChange={this.handleChange}>
                                <option>Pilih..</option>
                                <option>Islam</option>
                                <option>Kristen Protestan</option>
                                <option>Katolik</option>
                                <option>Hindu</option>
                                <option>Buddha</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <label className="labelInput">Status</label>
                            <select className="form-control" id="statusSelect" name="status" onChange={this.handleChange}>
                                <option>Pilih..</option>
                                <option>Kawin</option>
                                <option>Belum Kawin</option>
                            </select>
                        </div>    
                        <div className="form-group">
                            <label className="labelInput">Pekerjaan</label>
                            <input type="text" className="form-control" id="pekerjaanInput" name="pekerjaan" onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label className="labelInput">Kewarganegaraan</label>
                            <input type="text" className="form-control" id="wargaInput" name="kewarganegaraan" onChange={this.handleChange}/>
                        </div>
                        <button type="submit" onClick={this.handleSubmit} class="btn" style={{backgroundColor: "#9BDEAC", color: "white", marginBottom: "20px"}}>Submit</button>
                    </div>
                </div>
            </>
         );
    }
}
export default Employee;