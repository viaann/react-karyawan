import React, { Component } from 'react'
import "../style/Home.css"
import { Button, Modal } from 'react-bootstrap';
const axios = require('axios');

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            detailShow: false,
            updateModal: false,
            res: [],
            minValue: 0,
            maxValue: 0,
            position: "",
            nik: 0,
            tempById: null
        }
    } 

    close = () => {
        this.setState({
            detailShow: false,
            updateModal: false,
         })
    }

    getAllData = () => {
        axios.get('http://localhost:8080/join')
            .then(response => {
                this.setState({
                    res: response.data
                })
            })
            .catch(error => {
                console.log(error);
        })
    }

    onDetailModal = id => {
        this.setState({
            detailShow: true,
            nik: id
        })
    }

    onUpdateModal = id => {
        this.setState({
            updateModal: true,
            nik: id
        })
    }

    componentDidMount() {
        this.getAllData();
    }

    findByName = val => {
        const data = this.state.res;
        const inputNama = val.target.value.toLowerCase();
        const dataByName = data.filter(res  => res.nama.toLowerCase().includes(inputNama)) 
        this.setState({
            res: dataByName
        })
    }

    setMinValue = val => {
        this.setState({
            minValue: val.target.value 
        })
    }

    setMaxValue = val => {
        this.setState({
            maxValue: val.target.value 
        })
    }

    findBySalary = () => {
        const data = this.state.res;
        const minValue = parseInt(this.state.minValue);
        const maxValue = parseInt(this.state.maxValue);
        
        const dataBySalary = data.filter(res => {
            const value = res.salary.split(" ");
            const salary = parseInt(value[1]);
            return salary >= minValue && salary <= maxValue
        })

        this.setState({
            res: dataBySalary
        })
    }

    getPosition = val => {
        this.setState({
            position: val.target.value
        })
    }

    findByPosition = () => {
        const data = this.state.res;
        const position = this.state.position;
        const dataByPosition = data.filter(res => res.position === position)
        this.setState({
            res: dataByPosition
        })
    }

    render() {         
          return ( 
            <> 
            {/* Filter Input */}
                <div>
                    <div className="form-group ml-4 row">
                         <input type="text" className="form-control"  placeholder="Name" style={{ width: "15%", marginRight: "10px"}} onChange={val => this.findByName(val)}/>
                    </div>
                    <div className="form-group ml-4 row">
                        <input type="number" className="form-control" placeholder="0" step="500000" style={{ width: "10%", marginRight: "10px"}} onChange={val => this.setMinValue(val)}/>
                        <button type="submit" id="btnByName" className="btn btn-filter" style={{color: "#8ec6c5"}} disabled>To</button>
                        <input type="number" className="form-control" placeholder="500000" step="500000" style={{ width: "10%", marginRight: "10px"}} onChange={val => this.setMaxValue(val)}/>
                        <button type="submit" id="btnByName" className="btn btn-filter" onClick={this.findBySalary}>Search</button>
                    </div>
                    <div className="form-group ml-4 row">
                        <select className="form-control" style={{width: "10%", marginRight: "10px"}} onChange={val => this.getPosition(val)}>
                            <option>Pilih..</option>
                            <option>Manager</option>
                            <option>Karyawan</option>
                        </select>
                        <button type="submit" id="btnByName" className="btn btn-filter" onClick={this.findByPosition}>Search</button>
                    </div>
                </div> 
                    { this.state.res.map(data => {      
                        const { nik, position, jenis_kelamin, nama, salary, alamat } = data
                        return (
                            <div className="row ml-4" key={nik}>
                                <div className="card" style={{width: "18rem", marginTop: "30px"}} onClick={() => this.onDetailModal(nik)}>
                                    <div className="card-body">
                                        <h5 className="card-title">{nama}</h5>
                                        <h6 className="card-subtitle mb-2 text-muted">{nik}</h6>
                                        <div className="card-text text-body">{position}</div>
                                        <div className="card-text text-body">{jenis_kelamin}</div>
                                        <div className="card-text text-body">{salary}</div>
                                        <div className="card-text text-body">{alamat}</div>
                                    </div>
                                    <div className="card-body">
                                        <a  className="card-link text-success" onClick={this.onUpdateModal} >Update</a>
                                        <a  className="card-link text-danger" onClick={() => this.delete(nik)}>Delete</a>
                                    </div>
                                </div>
                            </div>
                          )
                    })}
                    { this.renderModal(this.state.nik) }
                    { this.update(this.state.nik) }
            </>
         );
    }

    renderModal = nik => {
        axios.get(`http://localhost:8080/employee/${nik}`)
        .then(response => {
            this.showModal(response.data)
        })
        .catch(error => {
            console.log(error);
        })
    }

    showModal = res  => {
           return (
            <Modal show={this.state.detailShow} onHide={this.close}>
                <Modal.Header closeButton>
                <Modal.Title>Detail Karyawn</Modal.Title>
                </Modal.Header>
                <div className="modal-body">${res.nik}</div>
                <Modal.Footer>
                <Button variant="secondary" >
                    Close
                </Button>
                </Modal.Footer>
            </Modal>     
        )
    }

    
    update = id => {
        return (
            <Modal show={this.state.updateModal} onHide={this.close}>
                <Modal.Header closeButton>
                <Modal.Title>Update Karyawan</Modal.Title>
                </Modal.Header>
                <div className="modal-body">{id}</div>
                <Modal.Footer>
                <Button variant="secondary" >
                    Close
                </Button>
                </Modal.Footer>
            </Modal> 
        )
    }

    delete = nik => {
        return (
           axios.delete(`http://localhost:8080/delete/${nik}`)
                .then(res => window.location.reload())
        )
    }
 
}

 
export default Home;