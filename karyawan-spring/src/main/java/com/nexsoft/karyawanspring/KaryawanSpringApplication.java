package com.nexsoft.karyawanspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KaryawanSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(KaryawanSpringApplication.class, args);
    }

}
