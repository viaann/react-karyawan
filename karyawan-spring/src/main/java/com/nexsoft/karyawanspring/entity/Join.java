package com.nexsoft.karyawanspring.entity;

public class Join {
    private int nik;
    private String alamat;
    private String nama;
    private String jenis_kelamin;
    private String position;
    private String salary;

    public Join(int nik, String alamat, String nama, String jenis_kelamin, String position, String salary) {
        this.nik = nik;
        this.nama = nama;
        this.jenis_kelamin = jenis_kelamin;
        this.position = position;
        this.salary = salary;
        this.alamat = alamat;
    }

    public int getNik() {
        return nik;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public String getSalary() {
        return salary;
    }

    public String getPosition() {
        return position;
    }
}
